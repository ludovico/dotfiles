# **My NixOS Configuration**

This is my personal NixOS configuration. It is a work in progress and I am still learning NixOS. I am using this configuration to learn Nix and to learn how to use NixOS to manage my personal computers.

> **Any damage caused by this configuration is not my responsibility. Use at your own risk.**

[![Made with Doom Emacs](https://img.shields.io/badge/Made_with-Doom_Emacs-blueviolet.svg?style=flat-square&logo=GNU%20Emacs&logoColor=white)](https://github.com/doomemacs/doom-emacs)
[![NixOS Unstable](https://img.shields.io/badge/NixOS-unstable-blue.svg?style=flat-square&logo=NixOS&logoColor=white)](https://nixos.org)

---

## **Screenshots**

![Screenshot](assets/ss.png)

---

## **Software I use on my personal computer**

- Wayland compositor: [Hyprland](https://hyprland.org)
- Wayland bar: [Waybar](https://github.com/Alexays/Waybar)
- Notification manager: [dunst](https://dunst-project.org)
- Editor: [Doomemacs](https://github.com/doomemacs/doomemacs)
- Terminal: [wezterm](https://github.com/wez/wezterm)
- Shell: [fish](https://fishshell.com)
- Browser: [firefox](https://www.mozilla.org/en-US/firefox)

---

## **Acknowledgment:**

These are the people who have helped me learn Nix and NixOS. I would not have been able to do this without them.

- [fufexan](https://github.com/fufexan)
- [misterio77](https://github.com/misterio77)
- [hlissner](https://github.com/hlissner)
- [sweenu](https://github.com/sweenu)
- [digga](https://digga.divnix.com)

---

## **Why Nix?**

Nix allows for easy to manage, collaborative, reproducible deployments. This means that once something is setup and configured once, it works forever. If someone else shares their configuration, anyone can make use of it.

This flake is configured with the use of [digga][digga].

[digga]: https://github.com/divnix/digga
